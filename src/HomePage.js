import React from "react";
import 'bootstrap/dist/css/bootstrap.css';
import './css/styles.scss';
import HeaderContainer from './components/header/HeaderContainer';
import BodyContainer from './components/body/BodyContainer';
import FootContainer from './components/footer/FootContainer';
function HomePage(props){
	return (
		<>
		<HeaderContainer />
		<BodyContainer />
		<FootContainer />
		</>
	);
}

export default HomePage;