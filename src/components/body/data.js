
export const pageList =
    [
        {
        "pageType": "Page",
        "id": "1",
        "sublist_id": "page_homepage",
        "sublist_label": "Homepage",
        "icon": ["fas", "image"]
        },{
        "pageType": "Category",
        "id": "2",
        "sublist_id": "page_category",
        "sublist_label": "Bombs and guns",
        "icon": ["fas", "image"],
        "child":[
            {
                "pageType": "product",
                "id": "3",
                "sublist_id": "page_page",
                "sublist_label": "Guns",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "4",
                "sublist_id": "page_page",
                "sublist_label": "Hand guns",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "8",
                "sublist_id": "page_page",
                "sublist_label": "Rifles",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "9",
                "sublist_id": "page_page",
                "sublist_label": "Pocket guns",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "10",
                "sublist_id": "page_page",
                "sublist_label": "Revolvers",
                "icon": ["fas", "image"]
            }]
    },{
        "pageType": "Category",
        "id": "11",
        "sublist_id": "page_category",
        "sublist_label": "Page Name",
        "icon": ["fas", "image"],
        "child":[
            {
                "pageType": "product",
                "id": "12",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "13",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "14",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "15",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "16",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "17",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "18",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "19",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "20",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "21",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "41",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "42",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "43",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "44",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "33",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "34",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "35",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "36",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "37",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "38",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            }]
    },{
        "pageType": "Category",
        "id": "5",
        "sublist_id": "page_category",
        "sublist_label": "Has Grandchildren",
        "icon": ["fas", "image"],
        "child":[
            {
                "pageType": "product",
                "id": "6",
                "sublist_id": "page_page",
                "sublist_label": "More Children",
                "icon": ["fas", "image"],
                "child":[
                    {
                        "pageType": "product",
                        "id": "33",
                        "sublist_id": "page_page",
                        "sublist_label": "Has a Child",
                        "icon": ["fas", "image"],
                        "child":[
                            {
                                "pageType": "product",
                                "id": "50",
                                "sublist_id": "page_page",
                                "sublist_label": "Far Down",
                                "icon": ["fas", "image"]
                            }
                        ]
                    }, {
                        "pageType": "product",
                        "id": "34",
                        "sublist_id": "page_page",
                        "sublist_label": "Product name",
                        "icon": ["fas", "image"]
                    }]
            }, {
                "pageType": "product",
                "id": "7",
                "sublist_id": "page_page",
                "sublist_label": "Page",
                "icon": ["fas", "image"]
            }]
    },{
        "pageType": "Category",
        "id": "22",
        "sublist_id": "page_category",
        "sublist_label": "Traps and Snares",
        "icon": ["fas", "image"],
        "child":[
            {
                "pageType": "product",
                "id": "23",
                "sublist_id": "page_page",
                "sublist_label": "Pit trap",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "24",
                "sublist_id": "page_page",
                "sublist_label": "Pig snare",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "25",
                "sublist_id": "page_page",
                "sublist_label": "Bear Traps",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "26",
                "sublist_id": "page_page",
                "sublist_label": "Leg Snares",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "27",
                "sublist_id": "page_page",
                "sublist_label": "Neck Snares",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "28",
                "sublist_id": "page_page",
                "sublist_label": "Finger Traps",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "29",
                "sublist_id": "page_page",
                "sublist_label": "Have a Heart Traps",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "30",
                "sublist_id": "page_page",
                "sublist_label": "Cage Traps",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "31",
                "sublist_id": "page_page",
                "sublist_label": "General Snares",
                "icon": ["fas", "image"]
            },{
                "pageType": "product",
                "id": "32",
                "sublist_id": "page_page",
                "sublist_label": "General Traps",
                "icon": ["fas", "image"]
            }]
    }
    ];


