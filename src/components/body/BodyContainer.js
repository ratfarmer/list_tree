import React, { useState, useEffect } from 'react';
import { pageList } from './data';
import Icon from '../Icon';

function BodyContainer() {
	
	const [open, setOpen] = useState(null);
	const [parent, setParent] = useState('');
	
	const [data, setData] = useState(pageList);
	var floorPlan;
	
	useEffect(() => {
		if(open){
			setData(open);
		}else{
			setData(pageList);
			setParent('');
			}
		},[open]);
	
	function linkClicker(e){
		e.preventDefault();
	}
	function handleLists(child, parent){
		setOpen(child);
		setParent(parent);
		//console.log(parent);
	}
		
	const Menu = ({data}) => {
		return (
			<ul>
				{parent && <div id="backLink" onClick={() => setOpen(0)}><Icon icon={'chevron-left'} />{parent.sublist_label}</div>}
				{
					data.map((m, i) => {
						return (
						<div key={m.id}>
						<li className={'parentMenu'}>
							<a className="list_title" onClick={linkClicker} href="home">{m.sublist_label}</a>
							
							{m.child && <div id={"parent-"+m.id} className="listIcon" onClick={() => handleLists(m.child,m)}><Icon icon={'chevron-right'} /></div>}
						</li>
						</div>
							);
						}
					)
				}
			</ul>
		);
	}
	
	const App = () => (	<Menu data={data} /> );
	floorPlan = JSON.stringify(data);
	return(
		<div id="bodyContainer" className="container">
			<div className="row">
				<div id="topNavContainer">
				<App />
				</div>
				{floorPlan}
			</div>
		</div>
	);
}



export default BodyContainer;